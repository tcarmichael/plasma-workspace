# translation of plasma_applet_org.kde.plasma.brightness.pot to esperanto
# Copyright (C) 2023 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma package.
# Oliver Kellogg <olivermkellogg@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-17 00:40+0000\n"
"PO-Revision-Date: 2024-05-04 13:23+0100\n"
"Last-Translator: Oliver Kellogg <olivermkellogg@gmail.com>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/ui/BrightnessItem.qml:29
#, kde-format
msgctxt "Backlight on or off"
msgid "Off"
msgstr "Malŝaltita"

#: package/contents/ui/BrightnessItem.qml:30
#, kde-format
msgctxt "Brightness level"
msgid "Low"
msgstr "Malalta"

#: package/contents/ui/BrightnessItem.qml:31
#, kde-format
msgctxt "Brightness level"
msgid "Medium"
msgstr "Meza"

#: package/contents/ui/BrightnessItem.qml:32
#, kde-format
msgctxt "Brightness level"
msgid "High"
msgstr "Alta"

#: package/contents/ui/BrightnessItem.qml:33
#, kde-format
msgctxt "Backlight on or off"
msgid "On"
msgstr "Ŝaltita"

#: package/contents/ui/BrightnessItem.qml:45
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/KeyboardColorItem.qml:77
#, kde-format
msgid "Follow accent color"
msgstr "Sekvi akcentkoloron"

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Brightness and Color"
msgstr "Brileco kaj Koloro"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Ekranbrileco ĉe %1%"

#: package/contents/ui/main.qml:77
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Klavara brileco ĉe %1%"

#: package/contents/ui/main.qml:82
#, kde-format
msgctxt "Status"
msgid "Night Light suspended; middle-click to resume"
msgstr "Noktolumo suspendita; mezklaku por daŭrigi"

#: package/contents/ui/main.qml:84
#, kde-format
msgctxt "Status"
msgid "Night Light suspended"
msgstr "Nokta Lumo suspendita"

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "Status"
msgid "Night Light at day color temperature"
msgstr "Nokta Lumo je taga kolortemperaturo"

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "Status"
msgid "Night Light at night color temperature"
msgstr "Nokta Lumo je nokta kolortemperaturo"

#: package/contents/ui/main.qml:96
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light in morning transition (complete by %1)"
msgstr "Nokta Lumo je matena transiro (plenumita ĝis %1)"

#: package/contents/ui/main.qml:98
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light in evening transition (complete by %1)"
msgstr "Nokta Lumo je vespera transiro (plenumita ĝis %1)"

#: package/contents/ui/main.qml:113
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light evening transition scheduled for %1"
msgstr "Vespera transiro de Noktlumo planita por %1"

#: package/contents/ui/main.qml:115
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light morning transition scheduled for %1"
msgstr "Matena transiro de Noktlumo planita por %1"

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Rulumu por alĝustigi ekranbrilecon"

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Middle-click to suspend Night Light"
msgstr "Mezklaku por suspend Noktolumon"

#: package/contents/ui/main.qml:210
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Agordi Noktlumon…"

#: package/contents/ui/NightLightItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Suspended"
msgstr "Suspendita"

#: package/contents/ui/NightLightItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Nedisponebla"

#: package/contents/ui/NightLightItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Ne ŝaltita"

#: package/contents/ui/NightLightItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Ne efektiviĝanta"

#: package/contents/ui/NightLightItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Ŝaltita"

#: package/contents/ui/NightLightItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Matena Transiĝo"

#: package/contents/ui/NightLightItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "Tago"

#: package/contents/ui/NightLightItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Vespera Transiĝo"

#: package/contents/ui/NightLightItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Nokto"

#: package/contents/ui/NightLightItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightLightItem.qml:122
#, kde-format
msgctxt "@action:button Night Light"
msgid "Suspend"
msgstr "Suspendi"

#: package/contents/ui/NightLightItem.qml:146
#, kde-format
msgid "Configure…"
msgstr "Agordi…"

#: package/contents/ui/NightLightItem.qml:146
#, kde-format
msgid "Enable and Configure…"
msgstr "Ŝalti kaj Agordi…"

#: package/contents/ui/NightLightItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Transiĝo al tago plenumita ĝis:"

#: package/contents/ui/NightLightItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Transiĝo al nokto planita por:"

#: package/contents/ui/NightLightItem.qml:176
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Transiĝo al nokto plenumita ĝis:"

#: package/contents/ui/NightLightItem.qml:178
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Transiĝo al tago planita por:"

#: package/contents/ui/PopupDialog.qml:66
#, kde-format
msgid "Display Brightness"
msgstr "Ekranbrileco"

#: package/contents/ui/PopupDialog.qml:97
#, kde-format
msgid "Keyboard Brightness"
msgstr "Klavara Brileco"

#: package/contents/ui/PopupDialog.qml:130
#, kde-format
msgid "Keyboard Color"
msgstr "Klavara Koloro"

#: package/contents/ui/PopupDialog.qml:140
#, kde-format
msgid "Night Light"
msgstr "Nokta Lumo"
