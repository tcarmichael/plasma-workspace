# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2023, 2024 Alexander Yavorsky <kekcuha@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-17 00:40+0000\n"
"PO-Revision-Date: 2024-02-04 19:14+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.4\n"

#: package/contents/ui/BrightnessItem.qml:29
#, fuzzy, kde-format
#| msgctxt "Night light status"
#| msgid "Off"
msgctxt "Backlight on or off"
msgid "Off"
msgstr "Отключена"

#: package/contents/ui/BrightnessItem.qml:30
#, kde-format
msgctxt "Brightness level"
msgid "Low"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:31
#, kde-format
msgctxt "Brightness level"
msgid "Medium"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:32
#, kde-format
msgctxt "Brightness level"
msgid "High"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:33
#, fuzzy, kde-format
#| msgctxt "Night light status"
#| msgid "On"
msgctxt "Backlight on or off"
msgid "On"
msgstr "Включена"

#: package/contents/ui/BrightnessItem.qml:45
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/KeyboardColorItem.qml:77
#, kde-format
msgid "Follow accent color"
msgstr ""

#: package/contents/ui/main.qml:62
#, kde-format
msgid "Brightness and Color"
msgstr "Яркость и цвет"

#: package/contents/ui/main.qml:74
#, kde-format
msgid "Screen brightness at %1%"
msgstr "Яркость экрана: %1%"

#: package/contents/ui/main.qml:77
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr "Яркость клавиатуры: %1%"

#: package/contents/ui/main.qml:82
#, kde-format
msgctxt "Status"
msgid "Night Light suspended; middle-click to resume"
msgstr ""

#: package/contents/ui/main.qml:84
#, fuzzy, kde-format
#| msgid "Night Light"
msgctxt "Status"
msgid "Night Light suspended"
msgstr "Ночная цветовая схема"

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "Status"
msgid "Night Light at day color temperature"
msgstr ""

#: package/contents/ui/main.qml:91
#, kde-format
msgctxt "Status"
msgid "Night Light at night color temperature"
msgstr ""

#: package/contents/ui/main.qml:96
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light in morning transition (complete by %1)"
msgstr ""

#: package/contents/ui/main.qml:98
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light in evening transition (complete by %1)"
msgstr ""

#: package/contents/ui/main.qml:113
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light evening transition scheduled for %1"
msgstr ""

#: package/contents/ui/main.qml:115
#, kde-format
msgctxt "Status; placeholder is a time"
msgid "Night Light morning transition scheduled for %1"
msgstr ""

#: package/contents/ui/main.qml:119
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Используйте колесо мыши для настройки яркости экрана"

#: package/contents/ui/main.qml:122
#, fuzzy, kde-format
#| msgid "Middle-click to toggle Night Light"
msgid "Middle-click to suspend Night Light"
msgstr "Включить или отключить ночную цветовую схему: щелчок средней кнопкой"

#: package/contents/ui/main.qml:210
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Настроить ночную цветовую схему…"

#: package/contents/ui/NightLightItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Suspended"
msgstr ""

#: package/contents/ui/NightLightItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Недоступна"

#: package/contents/ui/NightLightItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Не включена"

#: package/contents/ui/NightLightItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Не запущена"

#: package/contents/ui/NightLightItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr "Включена"

#: package/contents/ui/NightLightItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr "Утренний переход"

#: package/contents/ui/NightLightItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr "День"

#: package/contents/ui/NightLightItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr "Вечерний переход"

#: package/contents/ui/NightLightItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr "Ночь"

#: package/contents/ui/NightLightItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightLightItem.qml:122
#, kde-format
msgctxt "@action:button Night Light"
msgid "Suspend"
msgstr ""

#: package/contents/ui/NightLightItem.qml:146
#, kde-format
msgid "Configure…"
msgstr "Настроить…"

#: package/contents/ui/NightLightItem.qml:146
#, kde-format
msgid "Enable and Configure…"
msgstr "Включить и настроить…"

#: package/contents/ui/NightLightItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr "Переход к дневной схеме завершён в:"

#: package/contents/ui/NightLightItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr "Переход к ночной схеме запланирован в:"

#: package/contents/ui/NightLightItem.qml:176
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr "Переход к ночной схеме завершён в:"

#: package/contents/ui/NightLightItem.qml:178
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr "Переход к дневной схеме запланирован в:"

#: package/contents/ui/PopupDialog.qml:66
#, kde-format
msgid "Display Brightness"
msgstr "Яркость экрана"

#: package/contents/ui/PopupDialog.qml:97
#, kde-format
msgid "Keyboard Brightness"
msgstr "Яркость клавиатуры"

#: package/contents/ui/PopupDialog.qml:130
#, fuzzy, kde-format
#| msgid "Keyboard Brightness"
msgid "Keyboard Color"
msgstr "Яркость клавиатуры"

#: package/contents/ui/PopupDialog.qml:140
#, kde-format
msgid "Night Light"
msgstr "Ночная цветовая схема"

#~ msgctxt "Status"
#~ msgid "Night Light off"
#~ msgstr "Ночная цветовая схема отключена"

#~ msgctxt "Night light status"
#~ msgid "Off"
#~ msgstr "Отключена"

#~ msgctxt "Status; placeholder is a temperature"
#~ msgid "Night Light at %1K"
#~ msgstr "Цветовая температура ночной цветовой схемы: %1K"
